#!/bin/bash

curl https://gitlab.com/api/v4/projects/18440910/wikis/Digital-Workshopserie > workshops.json
curl https://gitlab.com/api/v4/projects/18440910/wikis/Internationell-omvärldsbevakning > internationellt.json
curl https://gitlab.com/api/v4/projects/18440910/wikis/Lista-med-delat-material > tips.json
curl https://gitlab.com/api/v4/projects/18440910/wikis/home > index.json

curl https://gitlab.com/api/v4/projects/21882815/repository/commits | jq '{"updated":.[0].created_at}' > update_time.json

# (. | @html)
cat workshops.json | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' | mustache - index.mustache > workshops.html
cat tips.json | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' | mustache - index.mustache > tips.html
cat internationellt.json | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' | mustache - index.mustache > internationellt.html

# markdown and time
cat index.json | jq -cr '.content' | marked -s | jq -Rs '{"content":.}' > index-html.json
jq -s '.[0] * .[1]' index-html.json update_time.json | mustache - index.mustache > index.html

# clean up and create dist
rm *json
mv *html ../public
