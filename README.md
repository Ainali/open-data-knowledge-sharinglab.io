<table align="center"><tr><td align="center" width="9999">
<img src="img/nosad.png" align="center" width="250" alt="Project icon">

[https://nosad.se](https://nosad.se) genereras från Markdown och hostas på gitlab.io.

![Personer](https://img.shields.io/badge/dynamic/json?color=brightgreen&label=Antal%20personer%20som%20bidrar&query=%24.length&url=https%3A%2F%2Fgitlab.com%2Fapi%2Fv4%2Fprojects%2F21882815%2Frepository%2Fcontributors&style=for-the-badge&logo=gitlab)

</td></tr></table>

# Om

Sajten är en statisk webbsida bestående av tre filer:

1. Innehåll - wiki.md
2. Html - index.html
3. Stilmall - bootstrap.min.css

Designmålet är att det ska vara enkelt att bidra med innehåll och sidan ska vara enkel att underhålla.

![Arkitektur](img/architechture.png)

## Utveckling (lokal dator)

Serva katalogen i valfri webbserver. Inkluderar en Dockerfil för att inte behöva installera en webbserver.

```bash
# Checkout
git clone https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io.git
cd open-data-knowledge-sharing.gitlab.io
# Bygg
docker build -t nosad .
# Kör
docker run -it -d -p 80:80 nosad
```

## Redaktionella förändringar

Om syftet är att enbart ändra innehållet på sajten kan ändringar göras i ett editeringsgränssnitt på GitLab. Läs nedan rutin för hur bidrag går till.

Till editeringssidan: https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io/-/edit/master/public/content/wiki.md

## Hur man bidrar

Oavsett om man använder git eller editeringssidan så sker bidrag via en **MergeRequest**.

> Viktigaste är att vara tydlig med att beskriva den förändring man skickar in. Det behöver inte vara svårare än en tydlig rubrik.

**Alternativ - GIT**

1. Clone repot: $ git clone https://gitlab.com/open-data-knowledge-sharing/open-data-knowledge-sharing.gitlab.io.git
3. Skapa en ny bransch: $ git checkout -b cool_new_design_with_awesome_content
2. Gör dina ändringar och testa
3. Pusha ändringen: $ git push origin cool_new_design_with_awesome_content
4. Gitlab kommer svara med en URL som du måste besöka för att skapa en MergeRequest. Besök sidan och se till att inkludera en tydlig beskrivning av ändringen.

**Alternativ - Editeringssidan**

1. Gör din ändring och tryck på commit.
2. Du kommer automatiskt ombes göra en beskrivning för att gå vidare och skicka in en MergeRequest.

**Gokännandeprocess**

Genom att skicka in en MergeRequest är det ägaren till repot som får välja att acceptera förändringen eller inte. Det gör att myndigheten som ansvarar för sidan har kontroll på vad som infogas och kan ta ansvar för den publicerade sidan.

Ett bidrag kan enbart ske om du kan svara JA på följande frågor:
1. Licensierar du ditt bidrag i enlighet med MIT-licensen?
2. Om du har utfört arbetet som en del för din arbetsgivare, har du deras tillåtelse att göra bidraget?

**Produktionssättning**

När ägaren av repot godkänner din MergeRequest så kopieras automatiskt innehållet i katalogen **public** till en webbserver på gitlab.io. Förfarandet är beskrivet i filen för CI/CD (.gitlab-ci.yml).

## Licens

MIT
