
importScripts('js/workbox-v6.5.1/workbox-sw.js');
importScripts('js/marked.min.js');

workbox.setConfig({
  modulePathPrefix: '/js/workbox-v6.5.1/',
  debug: false
});

const URLS = [
  "https://gitlab.com/api/v4/projects/18440910/wikis/Digital-Workshopserie",
  "https://gitlab.com/api/v4/projects/18440910/wikis/Internationell-omvärldsbevakning",
  "https://gitlab.com/api/v4/projects/18440910/wikis/Lista-med-delat-material"
];

const cacheName = 'wiki';

self.addEventListener('install', (event) => {
  const populateCache = async () => {
    const cache = await caches.open(cacheName);
    await cache.addAll(URLS);
  };

  event.waitUntil(populateCache());
});

const handlerNationell = async ({url, request, event, params}) => {

  const response = await fetch("https://gitlab.com/api/v4/projects/18440910/wikis/Internationell-omvärldsbevakning?render_html=true");
  const responseBody = await response.text();

 //var html = marked.parse(responseBody.content);

  return new Response(responseBody, {
    headers: { 'Content-Type': 'application/json' }
  });
};

const handlerWorkshops = async ({url, request, event, params}) => {

  const response = await fetch("https://gitlab.com/api/v4/projects/18440910/wikis/Digital-Workshopserie");
  const responseBody = await response.text();

 //var html = marked.parse(responseBody.content);

  return new Response(responseBody, {
    headers: { 'Content-Type': 'application/json' }
  });
};

const handlerTips = async ({url, request, event, params}) => {

  const response = await fetch("https://gitlab.com/api/v4/projects/18440910/wikis/Lista-med-delat-material");
  const responseBody = await response.text();

 //var html = marked.parse(responseBody.content);

  return new Response(responseBody, {
    headers: { 'Content-Type': 'application/json' }
  });
};

workbox.routing.registerRoute(

  /.*(?:gitlab)\.com.*$/,
  // Use a Stale While Revalidate caching strategy
  new workbox.strategies.StaleWhileRevalidate({
    // Put all cached files in a cache named 'assets'
    cacheName: cacheName,
    plugins: [
      // Ensure that only requests that result in a 200 status are cached
      new workbox.cacheableResponse.CacheableResponsePlugin({
        statuses: [200],
      }),
    ],
  }),
);

workbox.routing.registerRoute(

  ({url}) => url.pathname === '/api/partial/workshops',

  handlerWorkshops);

  workbox.routing.registerRoute(

    ({url}) => url.pathname === '/api/partial/tips',

    handlerTips);

workbox.routing.registerRoute(

  ({url}) => url.pathname === '/api/partial/internationellt',

  handlerNationell);
