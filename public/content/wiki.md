[_metadata_:url]:- "https://gitlab.com/open-data-knowledge-sharing/wiki/-/wikis/home"

Nätverkets (NOSAD) mål är att praktisk hjälpa och inspirera offentlig verksamhet att öka tillgängliggörandet och nyttjandet av öppna data. Över 40 timmar inspelat material, 100 delade länkar och dokument, ca 500 personer ingår i nätverket. Alla kan **delta**, **dela** och **diskutera**.

## Snabblänkar

1. Nätverksträffar
   * [Kalendarium - kommande evenemang](#kalendarium)
   * <a href="/workshops" data-navigo>Arkivet - ta del i efterhand</a>
2. Lästips
   * <a href="/tips" data-navigo>Nätverket rekommenderar</a>
   * <a href="/internationellt" data-navigo>Internationell omvärldsbevakning</a>
3. [Nyhetsbrev](#nyhetsbrev)
4. [Katalog öppen programvara](https://offentligkod.se/)

## Varför detta nätverk?

Vinsterna är många; öppen innovation; kostnadseffektiv digitalisering; kompetensdelning; samverkan som ökar chansen till interoperabilitet mellan system och verksamheter.

Vi tror på att gå från Ego till Eko. Att öppet samarbeta kring vår information för att bättre kunna fullfölja vårt uppdrag. Våra kunder delas av andra aktörer inom offentlig förvaltning. Våra kunders livshändelser involverar flertalet aktörer inom offentlig sektor, men också privat sektor. Genom att skapa förutsättningar att öppet dela information och teknik, skapas också möjligheter för innovation – inte genom att börja på ett blankt papper men genom att hämta inspiration, data och kunskap där den redan finns och förvaltas.

Därför vill vi med detta nätverk bjuda in till kunskapsdelning och samverkan kring hur användande och samutveckling av öppna data och tillika öppen källkod och öppna standarder kan bidra till dessa vinster. Vårt mål är att hålla en praktisk tyngdpunkt och hjälpa och inspirera myndigheter i att ta nästa steg för att öka påverkan och driva på nyttiggörandet av öppna data och öppen källkod.

## Kalendarium

Nätverket träffas regelbundet (via videolänk), första tisdagen varje månad. Vi välkomnar förslag på ämnen och presentationer. Öppna data, öppen källkod och öppna standarder är den röda tråden för alla workshops.

| Datum | Anmälan |
|---|---|
| 2022-03-29 | [Kompetensförsörjning och livslångt lärande](https://www.goto10.se/event/kompetensforsorjning-och-livslangt-larande/) |
| 2022-04-05 | [Öppna data för ett hållbart godstransportsystem](https://www.goto10.se/event/nosad-natverkande-kring-oppna-data-och-oppen-kallkod-5/) |

<a href="https://nosad.se/workshops" data-navigo>Arkivet - ta del av inspelningar och presentationer i efterhand</a>

## Nyhetsbrev

[Maila](mailto:maria.dalhage@digg.se) för att anmäla eller avanmäla dig till nyhetsbrevet. Skriv gärna “Prenumerera på nosads nyhetsbrev” eller “Avprenumerera på nosads nyhetsbrev” i ämnesraden.

## Nätverket rekommenderar

Omvärldsanalys och kunskapsdelning krävs för att lyckas med digitaliseringen. Nätverket sammanställer regelbundet delat material i formen "Nätverket rekommenderar". Vi välkomnar förslag på nytt material eller kompletterings/ändringsförslag.

<a href="/tips" data-navigo>Till nätverket rekommenderar</a>

## Diskussionsforum

För att kunna fortsätta diskussionen även utanför våra träffar, gör ett inlägg på vårt fråge- och diskussionsforum.

[Till diskussionsforumet](https://community.dataportal.se/)  

## Kontakt

[Kontakta nosad via mail](mailto:maria.dalhage@digg.se)

## Vem står bakom nätverket

[Arbetsförmedlingens JobTech](https://jobtechdev.se/), [DIGG](https://www.digg.se/), och [Internetstiftelsen](https://internetstiftelsen.se/) samarbetar tillsammans med [Trafiklab](https://www.trafiklab.se/), [RISE](https://www.ri.se/) och [SKR](https://skr.se/).

## Cookies och privacy

Målet är sajten inte ska spåra användaren på något sätt. Berätta vad vi kan förbättra.

* Inga cookies används
* Några utgående länkar leder till https://www.youtube-nocookie.com.
* Ingen loggning sker på servern.
* Ingen användardata samlas in för webbanalys.
